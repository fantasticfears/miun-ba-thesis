require 'csv'

filename_prefix = ARGV[0]
thing, _ = filename_prefix.split('-')

(0..9).each do |idx|
  name = "#{filename_prefix}#{idx}.csv"
  puts name

  # f = File.read(name).chomp(",\n")

  csv = CSV.read(name, headers: false, col_sep: ',', converters: :numeric)[0].delete_if(&:nil?)
  case thing.to_sym
  when :latency
    max, min, sum, *arr = csv
    arr.sort!
    unless sum == arr.inject(0){|s,x| s + x } && arr[0] == min && arr[-1] == max
      puts "ERROR!!!#{sum} #{arr.inject(0){|s,x| s + x }}|#{min} #{arr[0]}|#{max} #{arr[-1]}\n"
    else
      puts "Good"
    end
  when :throughput
    min = ARGV[1].to_i
    max = ARGV[2].to_i
    csv.each do |e|
      unless e <= max && e >= min
        puts "ERROR!!! #{e}"

        exit 0
      end
    end

    if csv.length > 120
      puts "ERROR!!! #{e}"

      exit 0
    end

    puts "Good"
  end

end
